package app

import (
	"github.com/jlaffaye/ftp"
	"io/ioutil"
)

func ConnectToFTP() (*ftp.ServerConn, error) {
	client, err := ftp.Dial(Configuration.Settings.FtpHost + ":" + Configuration.Settings.FtpPort)
	if err != nil {
		return nil, err
	}

	if err := client.Login(Configuration.Settings.FtpLogin, ""); err != nil {
		return nil, err
	}
	return client, nil
}

func ReadFile(c *ftp.ServerConn, path string) ([]byte, error) {
	r, err := c.Retr(path)
	if err != nil {
		panic(err)
	}
	defer r.Close()

	buf, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	return buf, nil
}


