package app

import (
	"fmt"
	//"gopkg.in/yaml.v2"
	"gopkg.in/yaml.v3"
	"os"
)

var Configuration = NewConfig("config.yml")

type Config struct {
	Settings         struct {
		ContainerName        string `yaml:"container-name"`
		FtpPath                 string `yaml:"ftp-path"`
		FtpHost              string `yaml:"ftp-host"`
		FtpLogin             string `yaml:"ftp-login"`
		FtpPort              string `yaml:"ftp-port"`
		GoroutinesAmount     int `yaml:"goroutines-amount"`
	} `yaml:"settings"`
}

func NewConfig(file string) *Config {
	var cfg Config
	err := readFile(file, &cfg)
	if err != nil {
		fmt.Printf("Unable to create conf: %v", err)
		os.Exit(2)
	}
	return &cfg
}

func readFile(file string, cfg *Config) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	return err
}
