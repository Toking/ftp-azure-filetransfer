package app

import (
	"context"
	"fmt"
	"github.com/Azure/azure-storage-blob-go/azblob"
	"net/url"
	"os"
	"sync"
	"time"
)

type ChannelResponse struct {
	Error error
}

func UploadBytesToBlob(b []byte, name string) (string, error) {
	azrKey, accountName, endPoint, container := GetAccountInfo()
	u, _ := url.Parse(fmt.Sprint(endPoint, container, "/", name[1:]))
	credential, errC := azblob.NewSharedKeyCredential(accountName, azrKey)
	if errC != nil {
		return "", errC
	}

	blockBlobUrl := azblob.NewBlockBlobURL(*u, azblob.NewPipeline(credential, azblob.PipelineOptions{}))

	ctx := context.Background()
	o := azblob.UploadToBlockBlobOptions{
		BlobHTTPHeaders: azblob.BlobHTTPHeaders{
			ContentType: "image/jpg",
		},
	}

	_, errU := azblob.UploadBufferToBlockBlob(ctx, b, blockBlobUrl, o)
	return blockBlobUrl.String(), errU
}

func GetAccountInfo() (string, string, string, string) {
	azrBlobAccountName, azrKey := os.Getenv("AZURE_STORAGE_ACCOUNT"), os.Getenv("AZURE_STORAGE_ACCESS_KEY")
	azrPrimaryBlobServiceEndpoint := fmt.Sprintf("https://%s.blob.core.windows.net/", azrBlobAccountName)
	azrBlobContainer := Configuration.Settings.ContainerName

	return azrKey, azrBlobAccountName, azrPrimaryBlobServiceEndpoint, azrBlobContainer
}

func UploadProccess(m map[string][]byte) error {
	// push file contents from memory to Azure
	var wg sync.WaitGroup
	batch := make(map[string][]byte)
	index := 0
	for key, content := range m {
		index++
		batch[key] = content
		if (index%Configuration.Settings.GoroutinesAmount == 0) || index == len(m) {
			errorChannel := make(chan *ChannelResponse, len(batch))
			for keyM, contentM := range batch {
				wg.Add(1)
				go func(k string, c []byte) {
					defer wg.Done()
					fmt.Println("Started uploading: ", k)
					u, err := UploadBytesToBlob(c, k)
					if err != nil {
						er := &ChannelResponse{Error: err}
						errorChannel <- er
						fmt.Println("Error during upload: ", err)
					}

					fmt.Println("Finished uploading to: ", u)
					fmt.Println("==========================================================")
				}(keyM, contentM)
			}
			wg.Wait()
			close(errorChannel)
			chanErr := <-errorChannel
			if chanErr != nil && chanErr.Error != nil {
				return chanErr.Error
			}
			batch = make(map[string][]byte)
		}
	}

	return nil
}

func GetBlobChangeDate(name string)  (*time.Time, error) {
	azrKey, accountName, endPoint, container := GetAccountInfo()
	u, err := url.Parse(fmt.Sprint(endPoint, container, "/", name[1:]))
	if err != nil {
		return nil, err
	}
	credential, errC := azblob.NewSharedKeyCredential(accountName, azrKey)
	if errC != nil {
		return nil, errC
	}

	blockBlobUrl := azblob.NewBlockBlobURL(*u, azblob.NewPipeline(credential, azblob.PipelineOptions{}))

	res, err := blockBlobUrl.GetProperties(context.Background(), azblob.BlobAccessConditions{}, azblob.ClientProvidedKeyOptions{} )
	if err != nil {
		return nil, err
	}
	t := res.LastModified()
	return &t, nil

}


