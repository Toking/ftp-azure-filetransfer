package app

import (
	"github.com/jlaffaye/ftp"
	"sync"
)

func GetAllFiles(files []*ftp.Entry, entries []*ftp.Entry, path string, second bool, client *ftp.ServerConn) ([]*ftp.Entry, []*ftp.Entry) {
	if second {
		var newArr []*ftp.Entry
		for _, entry := range entries {
			t, err := client.List(entry.Name)
			if err != nil {
				return nil, nil
			}

			var changed []*ftp.Entry
			for _, tt := range t {
				newName := tt
				newName.Name = entry.Name + "/" + newName.Name
				changed = append(changed, newName)
			}

			newArr = append(newArr, changed...)
		}
		entries = newArr
	}

	var folders []*ftp.Entry
	for _, entry := range entries {
		if entry.Type == ftp.EntryTypeFile {
			changed := entry
			if !second {
				changed.Name = path + "/" + changed.Name
			}
			files = append(files, changed)
		} else {
			continue
			changed := entry
			if !second {
				changed.Name = path + "/" + changed.Name
			}
			folders = append(folders, changed)
		}
	}

	if len(folders) > 0 {
		return GetAllFiles(files, folders, "", true, client)
	}

	return files, nil
}

func GetListOfChangedFiles(path string) (map[string][]byte, error) {
	var res []*ftp.Entry
	client, err := ConnectToFTP()
	if err != nil {
		return nil, err
	}

	entries, err := client.List(path)
	if err != nil {
		return nil, err
	}

	ftpFiles, _ := GetAllFiles([]*ftp.Entry{}, entries, path, false, client)


	//Get slice of entries to update/create
	var wg sync.WaitGroup
	var batch []*ftp.Entry
	for k, entry := range ftpFiles {
		batch = append(batch, entry)
		if (k%Configuration.Settings.GoroutinesAmount == 0 && k != 0) || k == len(ftpFiles) - 1 {
			entryChan := make(chan *ftp.Entry, len(batch))
			for _, batchEntry := range batch {
				wg.Add(1)
				go func(bE *ftp.Entry) {
					defer wg.Done()
					t, err := GetBlobChangeDate(bE.Name)
					if err != nil || t == nil || bE.Time.After(*t) {
						entryChan <- bE
						return
					}
					return
				}(batchEntry)
			}
			wg.Wait()
			close(entryChan)
			for l := 0; l < len(batch); l++ {
				en := <-entryChan
				res = append(res, en)
				continue
			}
			batch = nil
		}
	}

	m := make(map[string][]byte)
	for _, entry := range res {
		file, err := ReadFile(client, entry.Name)
		if err != nil {
			return nil, err
		}

		m[entry.Name] = file
	}

	return m, nil
}

func SyncFlow() error {
	//GET UPDATE FILES
	m, err := GetListOfChangedFiles(Configuration.Settings.FtpPath)
	if err != nil {
		return err
	}

	//UPLOAD NEW FILES
	if len(m) > 0 {
		err = UploadProccess(m)
		if err != nil {
			return err
		}
	}

	return nil
}