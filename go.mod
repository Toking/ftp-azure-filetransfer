module azure

go 1.16

require (
	github.com/Azure/azure-storage-blob-go v0.13.0
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/jlaffaye/ftp v0.0.0-20210307004419-5d4190119067
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
